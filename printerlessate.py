#ssh pi@coilbot2.internal.wibotic.com



from flask import Flask, flash, request, redirect, url_for, render_template
#from printer import Printer
import csv
import cgi, os
import cgitb; cgitb.enable()
import time
app = Flask(__name__)

#PRINTER = Printer(dimensions={'x':400, 'y':275, 'z':400}) #Original dimensions: 400, 275, 400

started_test = False
log_messages = []
default_test = 'All'
test_list = []
cycle_time=30; step_distance=2; stability_delay=90; min_latdist=5; max_latdist=80; min_longdist=5; max_longdist=80; x=0; y=0; z=0

@app.route('/', methods=['GET', 'POST'])
def start_app():
#    test_list = PRINTER.get_tests()
    running_test = default_test

    if request.method == 'POST':
        if request.form.get('cancel') is not None: # Cancel current test
            log_messages.insert(0,cancel_test())

        elif request.form.get('start') is not None: # Toggle between runnning/pausing the test
#
#            r, running = PRINTER.pause_resume()
            running_test = request.form.get('test-select') if request.form.get('test-select') is not None else default_test

            if r.status_code == 204:
                if not started_test: # First time start button is clicked for this test
                    started_test = True
                    log_messages.insert(0,'Starting test \"' + running_test + '\"')
                elif running:
                    log_messages.insert(0,'Resuming test \"' + running_test + '\"')
                else:
                    log_messages.insert(0,'Pausing test \"' + running_test + '\"')
            else:
                log_messages.insert(0,'Unable to start test (check connection?)')
        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'testing-center.html',
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist,
                xcurr = 2,
                ycurr = 3,
                zcurr = 5
                #xcurr = PRINTER.get_dimensions('x'),
                #ycurr = PRINTER.get_dimensions('y'),
                #zcurr = PRINTER.get_dimensions('z')
	        )


    return render_template(
        'index.html',
#        running=('PAUSE' if PRINTER.is_running() else 'START'),
#        testlist=test_list
#        selected_test=running_test,
        messages=log_messages,
        currcycletime = cycle_time,
        currstabdelay = stability_delay,
        currstepdist = step_distance,
        currminlatdist = min_latdist,
        currmaxlatdist = max_latdist,
        currminlongdist = min_longdist,
        currmaxlongdist = max_longdist
    )

@app.route('/control', methods=['GET', 'POST'])
def printer_control():
    if request.method == 'POST':
        if request.form.get('cancel') is not None:
            log_messages.insert(0,cancel_test())
        elif request.form.get('moverel') is not None:
            x_rel = request.form.get('x_rel') #if request.args.get('x_rel') is not None else 0
            y_rel = request.form.get('y_rel') #if request.args.get('y_rel') is not None else 0
            z_rel = request.form.get('z_rel') #if request.args.get('z_rel') is not None else 0
            log_messages.insert(0,'Moving printer to:' + str(x_rel) + ", "+ str(y_rel) + ", "+ str(z_rel))
#			status = PRINTER.move_rel(x_rel, y_rel, z_rel)
#			log_messages.insert(0,PRINTER.get_position())
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('movespec') is not None:
            x_spec = request.form.get('x_spec')
            y_spec = request.form.get('y_spec')
            z_spec = request.form.get('z_spec')
            log_messages.insert(0,'Moving printer to:' + str(x_spec) + ", "+ str(y_spec) + ", "+ str(z_spec))
#			status = PRINTER.move_spec(x_spec, y_spec, z_spec)
#			log_messages.insert(0,PRINTER.get_position())
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('home') is not None:
#			status = PRINTER.home()
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')
            log_messages.insert(0,'Moving printer to home location')

        elif request.form.get('center') is not None:
#			status = PRINTER.center()
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')
#			log_messages.insert(0,PRINTER.get_position())
            log_messages.insert(0,'Centering printer')

        elif request.form.get('y_back') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(0,-1,0)
#   		log_messages.insert(0,PRINTER.get_position())
#	    	if status.status_code != 204:
#		    	log_messages.insert(0,'Unable to move printer (check connection?)')


        elif request.form.get('y_forward') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(0,1,0)
#  		    if status.status_code != 204:
#			    log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('x_right') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(1,0,0)
#		    if status.status_code != 204:
#			    log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('x_left') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(-1,0,0)
#		    if status.status_code != 204:
#			    log_messages.insert(0,'Unable to move printer (check connection?)')
        elif request.form.get('home_xy') is not None:
#           status = PRINTER.home_xy()
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')
            log_messages.insert(0,'Homing xy axis')

        elif request.form.get('z_up') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(0,0,1)
#           if status.status_code != 204:
#	    		log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('z_down') is not None:
            log_messages.insert(0,'Moving printer')
#		    status = PRINTER.move_rel(0,0,-1)
#		    if status.status_code != 204:
#			    log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('home_z') is not None:
            log_messages.insert(0,'Homing z axis')
#           status = PRINTER.home_z()
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')


        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'testing-center.html',
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist
	        )

        return render_template(
    		'printer-control.html',
		    messages=log_messages
	    )
    else:
        return render_template(
    		'printer-control.html',
		    messages=log_messages
	    )

@app.route('/tests', methods=['GET', 'POST'])
def testing_center():
    global cycle_time; global step_distance; global stability_delay; global min_latdist; global max_latdist; global min_longdist; global max_longdist
    if request.method == 'POST':
        if request.form.get('cancel') is not None:
            print ("testing")
            log_messages.insert(0,cancel_test())

        elif request.form.get('pause') is not None:
#			status = PRINTER.pause_resume()
            log_messages.insert(0,'pausing')

        #general settings
        elif request.form.get('cycle_time') is not None or request.form.get('stability_delay') is not None or request.form.get('step_distance') is not None:
            print ("in general settings")
            tempcycle = request.form.get('cycle_time')
            tempdelay = request.form.get('stability_delay')
            tempstep = request.form.get('step_distance')
            if (tempcycle == None or tempcycle == '' or tempcycle == ' '):
                tempcycle = 30.0 #default value
            if (tempdelay == None or tempdelay == '' or tempdelay == ' '):
                tempdelay = 90.0 #default value
            if (tempstep == None or tempstep == '' or tempstep == ' '):
                tempstep = 2.0 #default value
            cycle_time = float(tempcycle); stability_delay = float(tempdelay); step_distance = float(tempstep)
            log_messages.insert(0,'cycle time: ' + str(cycle_time) + '    stability delay:   ' + str(stability_delay) + '     step distance:' + str(step_distance))

		#lateral values
        elif request.form.get('min_latdist') is not None or request.form.get('max_latdist') is not None:
            tempminlat = request.form.get('min_latdist'); tempmaxlat = request.form.get('max_latdist')
            if (tempminlat == None or tempminlat == '' or tempminlat == ' '):
                tempminlat = 5.0 #default value
            if (tempmaxlat == None or tempmaxlat == '' or tempmaxlat == ' '):
                tempmaxlat = 80 #default value
            min_latdist = float(tempminlat); max_latdist = float(tempmaxlat)
            log_messages.insert(0,'min_latdist: ' + str(min_latdist) + '     max_latdist: ' + str(max_latdist))

		#longitudinal values
        elif request.form.get('min_longdist') is not None or request.form.get('max_longdist') is not None:
            tempminlong = request.form.get('min_longdist'); tempmaxlong = request.form.get('max_longdist')
            if (tempminlong == None or tempminlong == '' or tempminlong == ' '):
                tempminlong = 5 #default value
            if (tempmaxlong == None or tempmaxlong == '' or tempmaxlong == ' '):
                tempmaxlong = 80 #default value
            min_longdist = float(tempminlong); max_longdist = float(tempmaxlong)
            log_messages.insert(0,'min_longdist: ' + str(min_longdist) + '     max_longdist: ' + str(max_longdist))

        elif request.form.get('lateralbutton') is not None:
            #status = PRINTER.center()
#           if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')
            print ('lateral test')
            current_latdist = min_latdist
            print (current_latdist)
#			status = PRINTER.move_rel(min_latdist,0,0)
            while current_latdist < max_latdist:
                time.sleep(float(stability_delay+cycle_time))
                current_latdist += step_distance
                if current_latdist > max_latdist:
                    current_latdist = max_latdist
                print (current_latdist)
                log_messages.insert(0, current_latdist)
#				status = PRINTER.move_rel(step_distance,0,0)
#   			if status.status_code != 204:
#	    			log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('longitudinalbutton') is not None:
#           status = PRINTER.center()
#			if status.status_code != 204:
#				log_messages.insert(0,'Unable to move printer (check connection?)')
            print ('longitudinal test')
            current_longdist = min_longdist
            print (current_longdist)
#			status = PRINTER.move_rel(0,min_longdist,0)
            while current_longdist < max_longdist:
                time.sleep(float(stability_delay+cycle_time))
                current_longdist += step_distance
                if current_longdist > max_longdist:
                    current_longdist = max_latdist
                print (current_longdist)
#				status = PRINTER.move_rel(0,step_distance,0)
#			    if status.status_code != 204:
#				    log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('bothbutton') is not None: #Tests lateral then longitudinal
            print ('testing both')
#           status = PRINTER.center()
            log_messages.insert(0,'Lateral test')
            print ('Lateral test')
            current_latdist = min_latdist
            print (current_latdist)
#			status = PRINTER.move_rel(min_latdist,0,0)
            while current_latdist < max_latdist:
                time.sleep(float(stability_delay+cycle_time))
                current_latdist += step_distance
                if current_latdist > max_latdist:
                    current_latdist = max_latdist
#				status = PRINTER.move_rel(step_distance,0,0)
#   			if status.status_code != 204:
#	    			log_messages.insert(0,'Unable to move printer (check connection?)')

            #Now testing longitudinal
            #status = PRINTER.center()
#   		if status.status_code != 204:
#	    		log_messages.insert(0,'Unable to move printer (check connection?)')
            current_longdist = min_longdist
            log_messages.insert(0,'Longitudinal test')
            print ("Longitudinal test")
            print (current_longdist)
#			status = PRINTER.move_rel(0,min_longdist,0)
            while current_longdist < max_longdist:
                time.sleep(float(stability_delay+cycle_time))
                current_longdist += step_distance
                if current_longdist > max_longdist:
                    current_longdist = max_latdist
                print (current_longdist)
#				status = PRINTER.move_rel(0,step_distance,0)
#	    			log_messages.insert(0,'Unable to move printer (check connection?)')

        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'testing-center.html',
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist
	        )

        return render_template(
		    'testing-center.html',
		    messages=log_message
		    testlist = test_list,
            currcycletime = cycle_time,
            currstabdelay = stability_delay,
            currstepdist = step_distance,
            currminlatdist = min_latdist,
            currmaxlatdist = max_latdist,
            currminlongdist = min_longdist,
            currmaxlongdist = max_longdist
	    )

    else:
        return render_template(
		    'testing-center.html',
		    messages=log_messages,
		    testlist = test_list
	    )



def cancel_test():
	started_test = False
#	if PRINTER.is_running():
#		PRINTER.cancel()
#	return 'Cancelling test'
#	else:
#		return 'No test running'

def get_selected(form, options):
    for option in options:
        print(option)
        if form.get(option) is not None: return option
    return default_test


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port =80)
