# General idea
---------------------------------------------
Create an interface that makes controlling the Coilbot easier.
ate.py - The main file that defines what each button on the web interface does
printerlessate.py - This file doesn't rely on printer.py and can be used as a way of testing different features for the webpage if both Coilbots are unavailable. It may not contain all the features that can be found on ate.py
printer.py - creates a printer object that is used in ate.py
templates - This folder has all the HTML files that are referenced. The HTML files rely on Bootstrap for many of the components.
(most of the explanation of the code can be found in the comments for each file)

## The interface itself
---------------------------------------------
There are 3 pages on the interface: Home, Coilbot Control, and Testing Center
Home - Displays all the current settings and positions, gives user option to home the Coilbot
Coilbot Control - Can move Coilbot to relative points or to specific points. Also can move Coilbot 1mm in any direction using arrow keys displayed on interface. User can also send Coilbot to various home locations
Testing Center - Can run lateral and/or longitudinal tests. User is able to control different settings like cycle time, stability delay, step distance, min/max longitudinal distance, and min/max lateral distance.

All three of these pages also display a log at the bottom of the screen which contains any messages regarding the Coilbot or its movements.

## How to start the interface
---------------------------------------------
1. SSH into one of the Coilbots' Raspberry Pis
2. Navigate to directory: testcoilbot_docker/
3. Run command: 'docker-compose up --build'
4. In a web browser navigate to http://coilbot.internal.wibotic.com:9999/ OR http://coilbot2.internal.wibotic.com:9999/ (it depends on which CoilBot is being used)
Alternatively (if images are not needed or being used):
1. Navigate to the folder in which these files are located
2. Run ate.py (or printerlessate.py)

## Things that currently don't work/need to be added
---------------------------------------------
- Multi-threading and queues need to be implemented in the code
- The pause/resume button doesn't function which can theoretically be solved once multi-threading and queues are added
- If multi-threading is added, the cancel button should be changed to rely on threads
- To make the interface more intuitive, the pages might need to be re-arranged
- The interface isn't exactly aesthetically pleasing
- Add a pop-up window/modal that opens the first time the user navigates to the web interface to remind the user that they need to home the Coilbot (move to 0,0,0)
- Change the speed at which the Coilbot moves to make things go faster

## Installation
---------------------------------------------
This uses Flask to create the webpage. Make sure Flask is installed before using. Use the following command to install flask:
  pip3 install Flask

Install requests:
  pip3 install requests
