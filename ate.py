''' TODO:
Fix architecture using threading and queues
Make pause/resume work
Pop up window to remind user to home printer before anything else
Rearrange pages if needed
Aesthtically pleasing

NOTE:
Throughout the code, there's a common command: 'request.form.get('[buttonName]')'
[buttonName] refers to the name of the button in the function's respective template/HTML file

'''


from flask import Flask, flash, request, redirect, url_for, render_template
import sys
from printer import Printer
import csv
import cgi, os
import cgitb; cgitb.enable()
import time
import requests
app = Flask(__name__)

#Creates the Printer object
PRINTER = Printer(dimensions={'x':400, 'y':275, 'z':400}) #Original dimensions: 400, 275, 400

started_test = False
log_messages = []
default_test = 'All'
test_list = ["longitudinal_test", "lateral_test", "both"]

#All the setting values, these are all the default values
cycle_time=30; step_distance=2; stability_delay=90; min_latdist=5; max_latdist=80; min_longdist=5; max_longdist=80; x=0; y=0; z=0; isPaused = False; isLateral = False; isLongitudinal = False

#Home page, uses index.html
@app.route('/', methods=['GET', 'POST']) #app route tells Flask what URL should trigger the function
def start_app():
    test_list = PRINTER.get_tests()
    running_test = default_test
    if request.method == 'POST':
        #'cancel' refers to what the button is called in the HTML file
        if request.form.get('cancel') is not None: # Cancel current test
            log_messages.append(cancel_test('index.html'))
        elif request.form.get('start') is not None: # Toggle between runnning/pausing the test
            r, running = PRINTER.pause_resume()
            running_test = request.form.get('test-select') if request.form.get('test-select') is not None else default_test
            if r.status_code == 204:
                if not started_test: # First time start button is clicked for this test
                    started_test = True
                    log_messages.append('Starting test \"' + running_test + '\"')
                elif running:
                    log_messages.append('Resuming test \"' + running_test + '\"')
                else:
                    log_messages.append('Pausing test \"' + running_test + '\"')
            else:
                log_messages.append('Unable to start test (check connection?)')
        #home0 refers to sending Coilbot to 0,0,0
        elif request.form.get('home0') is not None:
            status = PRINTER.home()
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving CoilBot to 0,0,0')

        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'index.html',
                #all of these variables are displayed on the HTML webpage
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist,
                currpos = PRINTER.get_position()
	        )

    #render_template() tells the code what HTML file it should use as well as provides any variables that are referenced in that HTML file
    return render_template(
        'index.html', #this is the HTML file's name for this specific section of the interface
        running=('PAUSE' if PRINTER.is_running() else 'START'),
        testlist=test_list,
        selected_test=running_test,
        messages=log_messages,
        currcycletime = cycle_time,
        currstabdelay = stability_delay,
        currstepdist = step_distance,
        currminlatdist = min_latdist,
        currmaxlatdist = max_latdist,
        currminlongdist = min_longdist,
        currmaxlongdist = max_longdist,
        currpos = PRINTER.get_position()
    )

#Printer control, can move printer around, uses printer-control.html
@app.route('/control', methods=['GET', 'POST'])
def printer_control():
    if request.method == 'POST':
        if request.form.get('cancel') is not None:
            log_messages.append(cancel_test('printer_control.html'))

        #Moving the printer relative to its current position
        elif request.form.get('moverel') is not None:
            #gets the coordinates that the user has inputted into the interface and stores them in variables
            x_rel = request.form.get('x_rel')
            y_rel = request.form.get('y_rel')
            z_rel = request.form.get('z_rel')
            status = PRINTER.move_rel(x_rel, y_rel, z_rel) #sends a command using the Printer class
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204: #Checks if the printer is connected and that there are no error codes
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer to: ' + str(x_rel) + ", "+ str(y_rel) + ", "+ str(z_rel))

        #Moving the printer to a specific point
        elif request.form.get('movespec') is not None:
            x_spec = request.form.get('x_spec')
            y_spec = request.form.get('y_spec')
            z_spec = request.form.get('z_spec')
            status = PRINTER.move_spec(x_spec, y_spec, z_spec)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer to: ' + str(x_spec) + ", "+ str(y_spec) + ", "+ str(z_spec))

        #Moves the printer to 0,8,0 (to accommodate for the thickness of the coils)
        elif request.form.get('offsethome') is not None:
            status = PRINTER.offsethome()
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving CoilBot to (0, 8, 0)')

        elif request.form.get('home0') is not None:
            status = PRINTER.home()
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving CoilBot to 0,0,0')

        #Center aligns the RX and TX coils, moves to the middle of the z and x axes
        elif request.form.get('center') is not None:
            log_messages.append('Centering printer')
            status = PRINTER.center()
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Centering printer')

        #Moves -1mm in the y-direction (moves the plate)
        elif request.form.get('y_back') is not None:
            status = PRINTER.move_rel(0,-1,0)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')
                log_messages.append(PRINTER.get_position())

        #Moves +1mm in the y-direction (moves the plate)
        elif request.form.get('y_forward') is not None:
            status = PRINTER.move_rel(0,1,0)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')

        #Moves 1mm right (moves the printhead)
        elif request.form.get('x_right') is not None:
            status = PRINTER.move_rel(1,0,0)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')

        #Moves 1mm left (moves the printhead)
        elif request.form.get('x_left') is not None:
            status = PRINTER.move_rel(-1,0,0)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')

        #Homes the x and y axes (sends them to 0, 0)
        elif request.form.get('home_xy') is not None:
            status = PRINTER.home_xy()
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Homing xy axis')

        #Moves 1mm up (moves the printhead)
        elif request.form.get('z_up') is not None:
            status = PRINTER.move_rel(0,0,1)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')

        #Moves 1mm down (moves the printhead)
        elif request.form.get('z_down') is not None:
            status = PRINTER.move_rel(0,0,-1)
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Moving printer')

        #Homes the z-axis
        elif request.form.get('home_z') is not None:
            status = PRINTER.home_z()
            log_messages.append(PRINTER.get_position())
            if status.status_code != 204:
                log_messages.append('Unable to move printer (check connection?)')
                log_messages.append('Status code: ' + str(status.status_code))
            else:
                log_messages.append('Homing z axis')

        #Clears the log
        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'printer-control.html',
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist,
                currpos = PRINTER.get_position()
	        )
        return render_template(
		    'printer-control.html', #name of the template/HTML file used
		    messages=log_messages,
		    testlist = test_list,
            currcycletime = cycle_time,
            currstabdelay = stability_delay,
            currstepdist = step_distance,
            currminlatdist = min_latdist,
            currmaxlatdist = max_latdist,
            currminlongdist = min_longdist,
            currmaxlongdist = max_longdist,
            currpos = PRINTER.get_position()
        )

    else:
        return render_template(
		    'printer-control.html',
		    messages=log_messages,
		    testlist = test_list,
            currcycletime = cycle_time,
            currstabdelay = stability_delay,
            currstepdist = step_distance,
            currminlatdist = min_latdist,
            currmaxlatdist = max_latdist,
            currminlongdist = min_longdist,
            currmaxlongdist = max_longdist,
            currpos = PRINTER.get_position()
        )

#Testing center, uses testing-center.html
@app.route('/tests', methods=['GET', 'POST'])
def testing_center():
    #Allows these variables to be changed depending on the settings the user wants
    global cycle_time; global step_distance; global stability_delay; global min_latdist; global max_latdist; global min_longdist; global max_longdist; global isLateral; global isLongitudinal #need this line to edit the variables
    if request.method == 'POST':
        if request.form.get('cancel') is not None:
            print ("testing")
            log_messages.append(cancel_test('testing-center.html'))

        elif request.form.get('pause') is not None:
            pause_resume()
            log_messages.append('pausing')

        #general settings - cycle time, stability delay, step distance
        elif request.form.get('cycle_time') is not None or request.form.get('stability_delay') is not None or request.form.get('step_distance') is not None:
            print ("in general settings")
            #creates temporary variables to store the preferred settings
            tempcycle = request.form.get('cycle_time')
            tempdelay = request.form.get('stability_delay')
            tempstep = request.form.get('step_distance')
            if (tempcycle == None or tempcycle == '' or tempcycle == ' '): #if the user didn't submit anything
                tempcycle = 30.0 #default value
            if (tempdelay == None or tempdelay == '' or tempdelay == ' '):
                tempdelay = 90.0 #default value
            if (tempstep == None or tempstep == '' or tempstep == ' '):
                tempstep = 2.0 #default value
            #sets the global variables to the temporary variables' values
            cycle_time = float(tempcycle); stability_delay = float(tempdelay); step_distance = float(tempstep)
            log_messages.append('Cycle time: ' + str(cycle_time) + '    Stability delay:   ' + str(stability_delay) + '     Step distance:' + str(step_distance))

		#lateral values - min and max lat distance
        elif request.form.get('min_latdist') is not None or request.form.get('max_latdist') is not None:
            tempminlat = request.form.get('min_latdist'); tempmaxlat = request.form.get('max_latdist')
            if (tempminlat == None or tempminlat == '' or tempminlat == ' '):
                tempminlat = 5.0 #default value
            if (tempmaxlat == None or tempmaxlat == '' or tempmaxlat == ' '):
                tempmaxlat = 80 #default value
            min_latdist = float(tempminlat); max_latdist = float(tempmaxlat)
            log_messages.append('Minimum lateral distance: ' + str(min_latdist) + '     Max lateral distance: ' + str(max_latdist))

		#longitudinal values - min and max long distance
        elif request.form.get('min_longdist') is not None or request.form.get('max_longdist') is not None:
            tempminlong = request.form.get('min_longdist'); tempmaxlong = request.form.get('max_longdist')
            if (tempminlong == None or tempminlong == '' or tempminlong == ' '):
                tempminlong = 5 #default value
            if (tempmaxlong == None or tempmaxlong == '' or tempmaxlong == ' '):
                tempmaxlong = 80 #default value
            min_longdist = float(tempminlong); max_longdist = float(tempmaxlong)
            log_messages.append('Minimum longitudinal distance: ' + str(min_longdist) + '     Max longitudinal distance: ' + str(max_longdist))

        #If the user selects the lateral button in the dropdown list
        elif request.form.get('lateralbutton') is not None:
            log_messages.append('Starting lateral test')
            lateral_test()
            log_messages.append('Ending lateral test')
            isLateral = False

        #If the user selects the longitudinal button in the dropdown list
        elif request.form.get('longitudinalbutton') is not None:
            log_messages.append('Starting longitudinal test')
            longitudinal_test()
            log_messages.append('Ending longitudinal test')
            isLongitudinal = False

        #If the user selects the "both" button in the dropdown list
        elif request.form.get('bothbutton') is not None: #Tests lateral then longitudinal
            log_messages.append('Starting lateral test')
            lateral_test()
            log_messages.append('Ending lateral test')
            #Now testing longitudinal
            log_messages.append('Starting longitudinal test')
            longitudinal_test()
            log_messages.append('Ending longitudinal test')
            isLateral = False; isLongitudinal = False

        elif request.form.get('clearlog') is not None:
            log_messages.clear()
            return render_template(
		        'testing-center.html',
		        messages=log_messages,
		        testlist = test_list,
                currcycletime = cycle_time,
                currstabdelay = stability_delay,
                currstepdist = step_distance,
                currminlatdist = min_latdist,
                currmaxlatdist = max_latdist,
                currminlongdist = min_longdist,
                currmaxlongdist = max_longdist,
                currpos = PRINTER.get_position()
	        )

        return render_template(
		    'testing-center.html',
		    messages=log_messages,
		    testlist = test_list,
            currcycletime = cycle_time,
            currstabdelay = stability_delay,
            currstepdist = step_distance,
            currminlatdist = min_latdist,
            currmaxlatdist = max_latdist,
            currminlongdist = min_longdist,
            currmaxlongdist = max_longdist,
            currpos = PRINTER.get_position()

	    )

    else:
        return render_template(
		    'testing-center.html',
		    messages=log_messages,
		    testlist = test_list,
            currcycletime = cycle_time,
            currstabdelay = stability_delay,
            currstepdist = step_distance,
            currminlatdist = min_latdist,
            currmaxlatdist = max_latdist,
            currminlongdist = min_longdist,
            currmaxlongdist = max_longdist,
            currpos = PRINTER.get_position()
	    )


def cancel_test(template_name):
    global min_latdist; global max_latdist; global min_longdist; global max_longdist
    #sets the min and max variables to 0 in case the cancel is run during the middle of a test, this prevents the printer from moving after it is cancelled
    min_latdist = 0; max_latdist = 0; min_longdist = 0; max_longdist = 0
    #homes the printer (moves to 0,0,0)
    status = PRINTER.home()
    #cancel = !cancel
    if status.status_code != 204:
       log_messages.append('Unable to move printer (check connection?)')
       log_messages.append('Status code: ' + str(status.status_code))
    #return cancel
    else:
        log_messages.append('Cancelling test')
    status = PRINTER.cancel()

    return render_template(
        template_name,
        messages=log_messages
    )
#Currently not functioning
def pause_resume():
    global isPaused; global min_latdist; global min_longdist
    x = PRINTER.get_x(); y = PRINTER.get_y(); z = PRINTER.get_z()
    print (isPaused)
    if isPaused == False: #if the printer is currently NOT paused, then move the printer to 0,0,0 and store the position it was at
        status = PRINTER.home()
    elif isPaused == True:
        status = PRINTER.move_spec(x,y,z)
#        if isLateral == True and isLongitudinal == False:
#            min_latdist = x
#            lateral_test()
#        elif isLateral == False and isLongitudinal == True:
#            min_longdist = y
#            longitudinal_test()
    isPaused = not isPaused
    print (isPaused)

#Runs the longitudinal tests
def longitudinal_test():
    global isLongitudinal
    isLongitudinal = True
    status = PRINTER.center() #center aligns the RX and TX coils
    if status.status_code != 204:
        log_messages.append('Unable to move printer (check connection?)')
        log_messages.append('Status code: ' + str(status.status_code))
    current_longdist = min_longdist #creates a variable that always stores the current longitudinal distance
    print (current_longdist)
    status = PRINTER.move_rel(0,min_longdist,0) #move to the minimum longitudinal distance
    #BAD, need to fix using threading and queues
    time.sleep(float(stability_delay+cycle_time)) #nothing happens for this period of time
    while current_longdist < max_longdist:
        #runs as long as the current location of the printer hasn't exceeded the max distance the printer is supposed to go
        time.sleep(float(stability_delay+cycle_time))
        current_longdist += step_distance #increase the current distance by the step distance
        if current_longdist > max_longdist:
            current_longdist = max_longdist
        print (current_longdist)
        status = PRINTER.move_rel(0,step_distance,0) #move the printer in the y direction by step_distance
        if status.status_code != 204:
            log_messages.append('Unable to move printer (check connection?)')
            log_messages.append('Status code: ' + str(status.status_code))
        if request.form.get('cancel') is not None:
            cancel_test('testing-center.html')
            log_messages.append('Cancelling test')

    return render_template(
        'testing-center.html',
        messages=log_messages,
        testlist = test_list,
        currcycletime = cycle_time,
        currstabdelay = stability_delay,
        currstepdist = step_distance,
        currminlatdist = min_latdist,
        currmaxlatdist = max_latdist,
        currminlongdist = min_longdist,
        currmaxlongdist = max_longdist,
        currpos = PRINTER.get_position()
    )

#Runs the lateral tests, similar concept to the longitudinal test function
def lateral_test():
    global isLateral
    isLateral = True
    status = PRINTER.center() #center align the RX and TX coils
    if status.status_code != 204:
        log_messages.append('Unable to move printer (check connection?)')
        log_messages.append('Status code: ' + str(status.status_code))
    current_latdist = min_latdist
    print (current_latdist)
    status = PRINTER.move_rel(min_latdist,0,0) #move to the minimum lateral distance
    time.sleep(float(stability_delay+cycle_time))
    while current_latdist < max_latdist:
        time.sleep(float(stability_delay+cycle_time))
        current_latdist += step_distance
        if current_latdist > max_latdist:
            current_latdist = max_latdist
        status = PRINTER.move_rel(step_distance,0,0)
        if status.status_code != 204:
            log_messages.append('Unable to move printer (check connection?)')
            log_messages.append('Status code: ' + str(status.status_code))
        if request.form.get('cancel') is not None:
            return cancel_test('testing-center.html')
    return render_template(
        'testing-center.html',
        messages=log_messages,
        testlist = test_list,
        currcycletime = cycle_time,
        currstabdelay = stability_delay,
        currstepdist = step_distance,
        currminlatdist = min_latdist,
        currmaxlatdist = max_latdist,
        currminlongdist = min_longdist,
        currmaxlongdist = max_longdist,
        currpos = PRINTER.get_position()
    )

def get_selected(form, options):
    for option in options:
        print(option)
        if form.get(option) is not None: return option
    return default_test


if __name__ == '__main__':
    app.run(host = "0.0.0.0", debug=True, port = 9999)
