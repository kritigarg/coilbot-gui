#Alex's code, defines all the printer commands that are referenced in ate.py

import requests
import json
from os.path import isfile
from time import sleep

#min and max speeds for the coilbot
MIN_SPEED = 25 # mm/min
MAX_SPEED = 8000 # mm/min

class Printer:
    def __init__(self, dimensions={'x':400, 'y':275, 'z':400}):
        self.API_KEY  = '38D1D27E3B134EFF89AEA54AF5FA8CF9' #needs to be consistent with everything, can be found in apikey.txt
        self.BASE_URL = 'http://octoprint/'
        self.headers = {'X-Api-Key':self.API_KEY, 'Content-Type':'application/json'}
        self.LONGITUDINAL_OFFSET = 8 #Physical mm between coils at y-home get_position


        self.all_tests = ['longitudinal_test', 'lateral_test', 'both']
        self.running_test_index = 0

        self.running = False

        # Current location of each axis
        self.x = 0; self.y = 0; self.z = 0
        self.dimensions = dimensions

        sleep(40) #sleeps for this amount of time to let the octoprint image finish setting up
        self.connect()


    #returns current position of the printer
    def get_position(self):
        return {'X':self.x, 'Y':self.y, 'Z':self.z}

    #returns the x position
    def get_x(self):
        return self.x

    #returns the y position
    def get_y(self):
        return self.y

    #returns the z position
    def get_z(self):
        return self.z

	#returns the dimension that the string specified
    def get_dimensions(self, str):
        if str == 'x':
            return self.dimensions['x']
        elif str == 'y':
            return self.dimensions['y']
        elif str == 'z':
            return self.dimensions['z']

	#returns true if running, false if not running
    def is_running(self):
        return self.running

    def get_tests(self):
        return self.all_tests

    def is_connected(self):
        get_from = self.BASE_URL + 'api/connection'
        r = requests.get(get_from, headers=self.headers)
        if r.status_code == 200:
            data = json.loads(r.content)
            return data['current']['state'] == 'Operational'
        else:
            return False

    def connect(self):
        post_to = self.BASE_URL + 'api/connection'
        data = {'command':'connect'}
        r = POST(post_to, data, headers=self.headers)
#        if r.status_code is not 200:
#            raise Exception("The status code is not 200")
        return r

    #moves the printer to points relative to its current location (adds x,y,z on to the current x,y,z coordinates)
    def move_rel(self, x=0, y=0, z=0, speed=4000):
        if x is None or x == ' ' or x == '': x = 0
        if y is None or y == ' ' or y == '': y = 0
        if z is None or z == ' ' or z == '': z = 0
        x = int(x); y = int(y); z = int(z)

        # Doesn't allow printer to move past maximum or negative
        if x > 0: x = max(0, min(x, self.dimensions['x'] - self.x))
        if y > 0: y = max(0, min(y, self.dimensions['y'] - self.y)) #was originally y - self.LONGITUDINAL_OFFSET
        if z > 0: z = max(0, min(z, self.dimensions['z'] - self.z))

        if x < 0: x = min(0, max(x, -self.x))
        if y < 0: y = min(0, max(y, self.y)) #was originally self.LONGITUDINAL_OFFSET + self.y
        if z < 0: z = min(0, max(z, -self.z))

        post_to = self.BASE_URL + 'api/printer/printhead'

        #creates a command that is sent to the printhead
        data = {'command':'jog', 'x':x, 'y':y, 'z':z, 'speed':speed}

        #posts the command to the api location
        r = POST(post_to, data, headers=self.headers)

        #gets the status code of r
        if r.status_code == 204: # Request successful
            time_to_sleep = 60 * float(max(abs(x), abs(y), abs(z))) / abs(speed)
            sleep(time_to_sleep)
            #updates the location of the printer
            self.x += x; self.y += y; self.z += z
        return r

    #moves the printer to specific coordinates as specificied by user
    def move_spec(self,x,y,z,speed):
        if x is None or x == ' ' or x == '': x = self.x
        if y is None or y == ' ' or y == '': y = self.y  # self.y- self.LONGITUDINAL_OFFSET
        if z is None or z == ' ' or z == '': z = self.z
        x = int(x); y = int(y); z = int(z)

        # Doesn't allow printer to move past maximum or negative
        x = max(0, min(x, self.dimensions['x']))
        y = max(0, min(y, self.dimensions['y']))
        z = max(0, min(z, self.dimensions['z']))

        post_to = self.BASE_URL + 'api/printer/printhead'
        data = {'command':'jog', 'absolute':True, 'x':x, 'y':y, 'z':z, 'speed':speed}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204: # Request successful
            #updates the location of the printer
            time_to_sleep = 60 * float(max(abs(x - self.x), abs(y - self.y), abs(z - self.z))) / abs(speed)
            sleep(time_to_sleep)
            self.x = x; self.y = y; self.z = z #y + self.LONGITUDINAL_OFFSET
        return r

    #moves the printer 0,8,0
    def offsethome(self, axes=['x', 'z']):
        post_to = self.BASE_URL + 'api/printer/printhead'
        data = {'command':'jog', 'absolute':True, 'x':0, 'y':self.LONGITUDINAL_OFFSET, 'z':0, 'speed':4000}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204:
            #updates the location of the printer
            self.x = 0 if 'x' in axes else self.x
            self.y = self.LONGITUDINAL_OFFSET if 'y' in axes else self.y
            self.z = 0 if 'z' in axes else self.z
        return r

    #moves the printer to 0,0,0
    def home(self, axes=['x', 'y', 'z']):
        post_to = self.BASE_URL + 'api/printer/printhead'
        #data = {'command':'jog', 'absolute':True, 'x':0, 'y':self.LONGITUDINAL_OFFSET, 'z':0, 'speed':1000}
        data = {'command':'home', 'axes':axes}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204:
            #updates the location of the printer
            self.x = 0; self.y = 0; self.z = 0
        return r

    #Center aligns the RX and TX coils, moves to the middle of the z and x axes
    def center(self, speed=4000):
        x_center = self.dimensions['x'] / 2 + 16
        z_center = self.dimensions['z'] / 2

        #sends the variables to the move_spec function
        return self.move_spec(x=x_center, y=8, z=z_center, speed=speed)

    #moves x axis to 0 and y axis to 8
    def home_xy(self, axes=['x', 'y']):
        post_to = self.BASE_URL + 'api/printer/printhead'
        data = {'command':'jog', 'absolute':True, 'x':0, 'y':self.LONGITUDINAL_OFFSET, 'speed':4000}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204:
            #updates the location of the printer
            self.x = 0; self.y = self.LONGITUDINAL_OFFSET
        return r

    #moves z axis to 0
    def home_z(self, axes=['z']):
        post_to = self.BASE_URL + 'api/printer/printhead'
        data = {'command':'home', 'axes':axes, 'speed':4000}

        r = POST(post_to, data, headers=self.headers)
        #updates the location of the printer
        if r.status_code == 204: self.z=0
        return r

    def cancel(self):
        #Cancel the running process entirely
        post_to = self.BASE_URL + 'api/job'
        data = {'command':'cancel'}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204: self.running = False
        return r

    #not working
    def pause_resume(self):
        # Toggle the current state of the printer. If running, pause. If paused, run
        currentposition = self.get_position()
        post_to = self.BASE_URL + 'api/job'
        data = {'command':'pause', 'action':'toggle'}

        r = POST(post_to, data, headers=self.headers)
        if r.status_code == 204: self.running = not self.running
        return r, self.running

def POST(url, data, headers=None):
    try: # If data is not valid JSON, this will throw a ValueError
        json.loads(data)
        json_data = data
    except:
        json_data = json.dumps(data)

    return requests.post(url=url, headers=headers, data=json_data)
